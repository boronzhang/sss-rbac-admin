package com.jwss.sra.framework.constant;

/**
 * 全局常量
 * @date 2022-2-9 14:27:00
 * @author jwss
 */
public class GlobalValue {
    /**
     * 系统启动时间
     */
    public static Long START_TIME;
}
